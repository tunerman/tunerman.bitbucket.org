/**
 * Иницилизация плагинов
 */

var myMap;
var myGeoObject;
var myPlacemark;
ymaps.ready(function () {
    myMap = new ymaps.Map("YMapsID", {
        center: [45.117314, 38.980383],
        zoom: 16,
        controls: ['smallMapDefaultSet']
    });

    myGeoObject = new ymaps.GeoObject({
        // Описание геометрии.
        geometry: {
            type: "Point",
            coordinates: [45.117314, 38.980383]
        },
        properties: {
            balloonContentBody: [
                '<address>',
                '<strong>Проект ВЫПУСКНОЙ — 2015!</strong>',
                '<br/>',
                'Баскет холл Краснодар, ул. Пригородная, 24',
                '<br/>',
                '8 (861) 244-1-244',
                '</address>'
            ].join('')
        }
    }, {
        iconLayout: 'default#image',
        iconImageHref: '/img/yandex/point.png',
        iconImageSize: [103, 58]
    });
    myMap.geoObjects.add(myGeoObject)
});

$(function() {
    /**
     * Слайдер
     */
    $('.slider').lightSlider({
        pager: false,
        //loop: true,
        autoWidth: true,
        slideMargin: 15
    });

    /**
     * Открытие модального окна
     */
    $(".j__open_window").on("click", function(e) {
        var $this = $(this);
        var handler = $this.data("link");

        $.arcticmodal({
            type: 'ajax',
            url: handler
        });

        e.preventDefault();
    });

    /**
     * Валидация формы
     */
    $(".j__form").on("submit", function(e) {

        var $form = $(this);
        var $requiredItem = $form.find(".form__field__required");
        var $errors = [];
        var formData = $form.serialize();

        return false;

        $requiredItem.each(function() {

            $(this).removeClass("form__field--error");

            if($(this).val() == '') {
                $errors.push($(this));
                $(this).addClass("form__field--error");
                rubmle($(this));
            }
        });

        if ($errors.length === 0) {
            $.arcticmodal({
                type: 'ajax',
                url: 'callback.php',
                afterClose: function() {
                    $.arcticmodal('close');
                    window.location.reload();
                },
                ajax: {
                    type: "post",
                    data: formData
                }
            });

            setTimeout(function(){
                window.location.reload();
            }, 4000);
        }

        function rubmle(el){
            var timeout;
            var $this = $(el);
            $this.jrumble({
                x: 2,
                y: 1,
                rotation: 0
            });
            clearTimeout(timeout);
            $this.trigger('startRumble');
            setTimeout(function(){$this.trigger('stopRumble')}, 150);
        }

        e.preventDefault();
    });

});
